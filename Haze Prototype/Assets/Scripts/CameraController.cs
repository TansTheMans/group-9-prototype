﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Transform player;
	private Vector3 offset;

	public float smoothSpeed = .15f;

	private void Start()
	{
		// player = GameObject.FindGameObjectWithTag("Player").transform;
		offset = this.transform.position - player.position;
	}

	private void LateUpdate()
	{
		Vector3 newPosition = player.position + offset;
		this.transform.position = Vector3.Lerp(transform.position, newPosition, smoothSpeed);
	}
}

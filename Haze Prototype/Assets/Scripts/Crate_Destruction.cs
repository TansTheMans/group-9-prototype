﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Crate_Destruction : MonoBehaviour
{
	Collider2D collider;

	void Start()
	{
		collider = GetComponent<Collider2D>();
	}

	void Update()
	{
		if (PlayerManager.instance.isIce == true)
		{
			collider.isTrigger = true;
		}
		else
		{
			collider.isTrigger = false;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Ice")
		{
			Destroy(this.gameObject);
		}
	}
}

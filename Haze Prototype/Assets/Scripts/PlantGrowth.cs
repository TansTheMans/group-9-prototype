﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class PlantGrowth : MonoBehaviour
{
	public SpriteRenderer spriteObject;

	public int growthValue = 0;

	public bool isHarvestable = false; // Variable to see if near a tree

	// Stages go from 0 (seed) to 4 (fully grown)
	// Stage 1 (seedling) Variables
	public int growthThreshold1 = 1;
	public Vector3 Scale1 = new Vector3 (1.2f, 1.2f, 1);
	public float Yinc1 = .2f;
	public Sprite sprite1;

	// Stage 2 (primary growth) Variables
	public int growthThreshold2 = 2;
	public Vector3 Scale2 = new Vector3(1.5f, 1.5f, 1);
	public float Yinc2 = .3f;
	public Sprite sprite2;

	// Stage 3 (secondary growth) Variables
	public int growthThreshold3 = 3;
	public Vector3 Scale3 = new Vector3(1.7f, 1.7f, 1);
	public float Yinc3 = .2f;
	public Sprite sprite3;

	// Stage 4 (maturity) Variables
	public int growthThreshold4 = 4;
	public Vector3 Scale4 = new Vector3(2f, 2f, 1);
	public float Yinc4 = .3f;
	public Sprite sprite4;



	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Player") && PlayerManager.instance.state == 2) 
		{
			Grow();
		}
		if (collision.gameObject.CompareTag("Player") && PlayerManager.instance.state == 2)
		{
			Shrink();
		}
		if (collision.gameObject.CompareTag("Player")  && PlayerManager.instance.isDash)
		{
			//isHarvestable = true;
			Harvest();
		}
	}

	void Grow()
	{
		growthValue += 1;
		if (growthValue == growthThreshold1)
		{
			spriteObject.sprite = sprite1; // change sprite to Stage 1 sprite
			this.transform.localScale = Scale1;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc1), this.transform.position.z);
			isHarvestable = true;
		}
		else if (growthValue == growthThreshold2)
		{
			spriteObject.sprite = sprite2; // change sprite to Stage 2 sprite
			this.transform.localScale = Scale2;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc2), this.transform.position.z);
		}
		else if (growthValue == growthThreshold3)
		{
			spriteObject.sprite = sprite3; // change sprite to Stage 3 sprite
			this.transform.localScale = Scale3;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc3), this.transform.position.z);
		}
		else if (growthValue == growthThreshold4)
		{
			spriteObject.sprite = sprite4; // change sprite to Stage 4 sprite
			this.transform.localScale = Scale4;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc4), this.transform.position.z);
		}
	}
	void Shrink()
	{
		growthValue -= 1;
		if (growthValue == growthThreshold1)
		{
			spriteObject.sprite = sprite1; // change sprite to Stage 1 sprite
			this.transform.localScale = Scale1;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc1), this.transform.position.z);
			isHarvestable = true;
		}
		else if (growthValue == growthThreshold2)
		{
			spriteObject.sprite = sprite2; // change sprite to Stage 2 sprite
			this.transform.localScale = Scale2;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc2), this.transform.position.z);
		}
		else if (growthValue == growthThreshold3)
		{
			spriteObject.sprite = sprite3; // change sprite to Stage 3 sprite
			this.transform.localScale = Scale3;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc3), this.transform.position.z);
		}
		else if (growthValue == growthThreshold4)
		{
			spriteObject.sprite = sprite4; // change sprite to Stage 4 sprite
			this.transform.localScale = Scale4;
			this.transform.position = new Vector3(this.transform.position.x, (this.transform.position.y + Yinc4), this.transform.position.z);
		}
	}

	public void Harvest() // Harvests tree branches
	{
		print("Harvest");
		if (isHarvestable == true) // Player collider hits tree collider...
		{
			if (growthValue == 0) // If the tree has grown from the seed...
			{
				PlayerManager.instance.storedBranches += 0; // Add branches
				//Shrink(); // Decrease tree size
				print("Harvest function fully works");
			}
			if (growthValue == 1) // If the tree has grown from the seed...
			{
				PlayerManager.instance.storedBranches += 1; // Add branches
				Shrink(); // Decrease tree size
				print("Harvest function fully works");
			}
			if (growthValue == 2) // If the tree has grown from the seed...
			{
				PlayerManager.instance.storedBranches += 1; // Add branches
				Shrink(); // Decrease tree size
				print("Harvest function fully works");
			}
			if (growthValue == 3) // If the tree has grown from the seed...
			{
				PlayerManager.instance.storedBranches += 2; // Add branches
				growthValue -= 1; // Decrease tree size
				print("Harvest function fully works");
			}
			if (growthValue == 4) // If the tree has grown from the seed...
			{
				PlayerManager.instance.storedBranches += 2; // Add branches
				Shrink(); // Decrease tree size
				print("Harvest function fully works");
			}
			if (growthValue == 5) // If the tree has grown from the seed...
			{
				PlayerManager.instance.storedBranches += 3; // Add branches
				Shrink(); // Decrease tree size
				print("Harvest function fully works");
			}
		}
	}

	/*public void MakeHarvestable() // Switches isHarvestable from false to true
    {
		isHarvestable = true;
    }*/
}

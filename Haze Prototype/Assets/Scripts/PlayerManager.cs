﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
	#region Singleton
	public static PlayerManager instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			// DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	#endregion

	// Variables for the Player and the state sprites & physics materials
	public Rigidbody2D playerRB;
	public GameObject iceSprite;
	public GameObject vaporSprite;
	public GameObject waterSprite;
	public PhysicsMaterial2D icePhysics;
	public PhysicsMaterial2D vaporPhysics;
	public PhysicsMaterial2D waterPhysics;

	// general attributes accross objects
	public float speed;
	public float jumpStrength;
	public float vaporDash;
	public float time = 10; // time for the jump cooldown
	public GameObject coolText;
	public int state = 0; // 0 = vapor, 1 = ice, 2 = water
	public Transform feet;
	public LayerMask groundLayers;
	public Transform waterReset;
	public GameObject thePlayer;

	// specific attributes for each state
	public float iceGravity = 1f;
	public float vaporGravity = .2f;
	public float waterGravity = 1f;
	public float iceMass = 2f;
	public float vaporMass = 1f;
	public float waterMass = 1.5f;
	public bool isDash = false;
	public bool isIce = false;
	public bool noJump = false;

	// Store of Seeds
	public int storedSeeds = 0;
	public GameObject seedTrack;

	//Store of Branches
	public int storedBranches = 0;
	public GameObject branchesTrack;

	//Calls the Plant growth script for the harvest function
	public PlantGrowth plantGrowth;

	// Text Objects for State UI
	public Text qButton;
	public Text eButton;

	// Objects for Dash UI
	public Image ButtonOutline;
	public Text ButtonText;
	public Text ButtonUseText;


	// Update is called once per frame
	void FixedUpdate()
    {
		if (state == 0 || state == 2) // handles all vapor state physics
		{
			float moveHorizontal = Input.GetAxis("Horizontal"); // handles horizontal movement input

			Vector2 movement = new Vector2(moveHorizontal, 0);

			playerRB.AddForce(movement * speed); // moves the player based on horizontal movement and speed
		}
    }

	private void Update()
	{
		if (Input.GetKeyDown("q")) // checks to see if the q button is pressed (will include e button to go other way but since only two states are in, I'll add it later)
		{
			state += 1; // changes to next state
			if (state == 3) // checks to see if the state is over the set possibilities and loops if it is
			{
				state = 0;
			}

			StateChange();
		}
		else if (Input.GetKeyDown("e"))
		{
			state -= 1; // changes to previous state
			if (state == -1) // checks to see if the state is over the set possibilities and loops if it is
			{
				state = 2;
			}

			StateChange();
		}

		if (state == 0) // handles all vapor state physics
		{
			if (Input.GetKeyDown(KeyCode.Space) && IsGrounded() && noJump == false) // checks to see if spacebar is pressed (currently not stopping holding down the button, need to fix)
			{
				StopCoroutine("JumpCool");

				Vector2 jump = new Vector2(0, jumpStrength);

				playerRB.AddForce(jump); // causes the player to jump

				StartCoroutine("JumpCool");
			}

			if (Input.GetKeyDown(KeyCode.LeftShift))
			{
				StopCoroutine("DashTime");

				// multiplies by movement input to determine direction (left or right)
				Vector2 dash = new Vector2((vaporDash * Input.GetAxis("Horizontal")), 0);

				playerRB.AddForce(dash); // adds a quick burst of speed

				//plantGrowth.Harvest(); // calls the harvest function from PlantGrowth Script

				// isDash = true;
				StartCoroutine("DashTime");
			}
		}

		//coolText.GetComponent<Text>().text = "Jump Cooldown: " + time;
		seedTrack.GetComponent<Text>().text = "Seeds: " + storedSeeds;
		branchesTrack.GetComponent<Text>().text = "Branches: " + storedBranches;
	}

	public bool IsGrounded()
    {
		Collider2D groundCheck = Physics2D.OverlapCircle(feet.position, 0.75f, groundLayers);

		if (groundCheck != null)
        {
			return true;
        }

		return false;
    }

	public void StateChange()
	{
		switch (state)
		{
			case 0: // activates the vapor state and makes sure it is in the right position
				iceSprite.SetActive(false);
				isIce = false;
				waterSprite.SetActive(false);
				vaporSprite.SetActive(true);
				speed = 5f;
				playerRB.gravityScale = vaporGravity;
				playerRB.sharedMaterial = vaporPhysics;
				playerRB.mass = vaporMass;

				qButton.text = "Ice";
				eButton.text = "Water";
				break;
			case 1: // activates the ice state and makes sure it is in the right position
				iceSprite.SetActive(true);
				isIce = true;
				vaporSprite.SetActive(false);
				waterSprite.SetActive(false);
				playerRB.gravityScale = iceGravity;
				playerRB.sharedMaterial = icePhysics;
				playerRB.mass = iceMass;

				qButton.text = "Water";
				eButton.text = "Vapor";
				break;
			case 2: // activates the water state and makes sure it is in the right position
				iceSprite.SetActive(false);
				isIce = false;
				vaporSprite.SetActive(false);
				waterSprite.SetActive(true);
				speed = 6.25f;
				playerRB.gravityScale = waterGravity;
				playerRB.sharedMaterial = waterPhysics;
				playerRB.mass = waterMass;
				// thePlayer.transform.position = waterReset.transform.position;
				// Instantiate(waterSprite, waterReset.transform.position, waterReset.transform.rotation);

				qButton.text = "Vapor";
				eButton.text = "Ice";
				break;
		}
	}

	public void FadeUI()
	{
		ButtonOutline.color = ButtonOutline.color - new Color(0, 0, 0, .7f);
		ButtonText.color = ButtonText.color - new Color(0, 0, 0, .7f);
		ButtonUseText.color = ButtonUseText.color - new Color(0, 0, 0, .7f);
	}

	public void UnfadeUI()
	{
		ButtonOutline.color = ButtonOutline.color + new Color(0, 0, 0, .7f);
		ButtonText.color = ButtonText.color + new Color(0, 0, 0, .7f);
		ButtonUseText.color = ButtonUseText.color + new Color(0, 0, 0, .7f);
	}

	IEnumerator DashTime()
	{
		print("In Dash Time");
		isDash = true;
		FadeUI();

		yield return new WaitForSeconds(1);

		print("Exiting Dash Time");
		isDash = false;
		UnfadeUI();
	}

	IEnumerator JumpCool()
	{
		print("Jump cooldown starts...");
		noJump = true;

		yield return new WaitForSeconds(time);

		print("...End of cooldown");
		noJump = false;
	}
}

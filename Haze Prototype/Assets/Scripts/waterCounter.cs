﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterCounter : MonoBehaviour
{
    public float waterPoints;
    public float maxWaterPoints = 5;

    void Start()
    {
        waterPoints = maxWaterPoints;
    }

    public void gainWater(float absorb)
    {

        waterPoints -= absorb;
        if (waterPoints <= 0)
        {
            Destroy(gameObject);
        }
    }
}

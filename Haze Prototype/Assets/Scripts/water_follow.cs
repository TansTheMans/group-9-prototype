﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class water_follow : MonoBehaviour
{
    public float speed;
    public float stoppingDistance;
    public Transform target;
    public int state = 0;

    // Start is called before the first frame update
    void Start()
    {
       // target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, target.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (Input.GetKeyDown("3"))
        {
            state += 1;
            if (state == 2)
            {
                state = 0;
            }

            Split();
        }
    }
    public void Split()
    {
        switch (state)
        {
            case 0: // water trails at normal speed
                speed = 5f;

                break;
            case 1: // Brings trailing water speed to 0
                speed = 0f;
                break;
        }
    }
}

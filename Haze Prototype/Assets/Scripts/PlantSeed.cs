﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantSeed : MonoBehaviour
{
	public GameObject tree;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Player") && PlayerManager.instance.storedSeeds > 0)
		{
			Instantiate(tree, this.transform.position, this.transform.rotation);
			PlayerManager.instance.storedSeeds -= 1;
			Destroy(this);
		}
	}
}

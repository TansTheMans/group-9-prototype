﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debrisDestroy : MonoBehaviour
{
    private bool dirRight = true;
    public float speed = 6.0f;

    void Update()
    {
        if (dirRight)
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        else
            transform.Translate(-Vector2.right * speed * Time.deltaTime);

        if (transform.position.x >= 4.0f)
        {
            dirRight = false;
        }

        if (transform.position.x <= -4)
        {
            dirRight = true;
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "Ice_Block(Clone)")
        {
            Debug.Log("Boom");
            Destroy(gameObject);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterDrop : MonoBehaviour
{
    public GameObject water;
    public Transform dropArea;

    void Update()
    {
        if (Input.GetKeyDown("4"))
        {
            Instantiate(water, dropArea.transform.position, dropArea.transform.rotation);
        }
    }
}

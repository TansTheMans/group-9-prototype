﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class modelSwap : MonoBehaviour
{
    public GameObject m1;
    public GameObject m2;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("1"))
        {
            m1.SetActive(true);
            m2.SetActive(false);
        }
        if (Input.GetButtonDown("2"))
        {
            m1.SetActive(false);
            m2.SetActive(true);
        }
    }
}

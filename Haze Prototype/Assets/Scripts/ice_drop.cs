﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ice_drop : MonoBehaviour
{
    public GameObject ice;
    public Transform dropArea;

    void Update()
    {
        if (Input.GetKeyDown("5"))
        {
            Instantiate(ice, dropArea.transform.position, dropArea.transform.rotation);
        }
    }
}
